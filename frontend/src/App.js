import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import SignIn from "./containers/SignIn";
import SignUp from "./containers/SignUp";
import {ADD_PLACE_PATH, LOGIN_PATH, PLACE_INFO_PATH_ID, REG_PATH} from "./constants/paths";
import PlacesContainer from "./containers/PlacesContainer/PlacesContainer";
import PlaceFormContainer from "./containers/PlaceFormContainer/PlaceFormContainer";
import PlaceReviewContainer from "./containers/PlaceReviewContainer/PlaceReviewContainer";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {useSelector} from "react-redux";

const App = () => {
  const profile = useSelector((state) => state.profile.data)
  return (
    <Layout>
      <Switch>
        {profile && (
          <Route path={ADD_PLACE_PATH} component={PlaceFormContainer}/>
        )}
        <Route path={PLACE_INFO_PATH_ID} component={PlaceReviewContainer}/>
        <Route path={LOGIN_PATH} component={SignIn}/>
        <Route path={REG_PATH} component={SignUp}/>
        <Route path={"*"} component={PlacesContainer}/>
      </Switch>
    </Layout>
  );
};

export default App;
