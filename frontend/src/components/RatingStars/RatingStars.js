import React, {useCallback, useState} from 'react';
import {Rating} from "@mui/material";

const RatingStars = ({onSubmit, name, onStarClick, defaultValue, precision, readOnly = false}) => {
  const [starsVal, setStarsVal] = useState(defaultValue || 0);
  const onStarChange = useCallback((e) => setStarsVal(parseFloat(e.target.value)), [])
  return (
    <Rating
      onChange={onStarChange}
      value={starsVal}
      precision={precision || 0.5}
      onSubmit={onSubmit}
      onClick={onStarClick}
      name={name}
      readOnly={readOnly}
    />
  );
};

export default RatingStars;