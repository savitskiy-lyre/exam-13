import React from 'react';
import {AppBar, Button, Divider, Grid, Stack, Toolbar} from "@mui/material";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import ProfileMenu from "../ProfileMenu/ProfileMenu";
import {BASE_URL, GALLERY_LOCATION} from "../../config";
import EmptyAvatar from "../../assets/images/empty_avatar.png";
import {userLogout} from "../../store/actions/usersActions";
import {ADD_PLACE_PATH, HOME_PATH, LOGIN_PATH, REG_PATH} from "../../constants/paths";
import {makeStyles} from "@mui/styles";

const AppToolbar = () => {
  const s = useStyles();
  const profile = useSelector((state) => state.profile.data);
  const dispatch = useDispatch();

  return (
    <>
      <AppBar position={"fixed"} color={'customBlack'}>
        <Toolbar>
          <Grid container justifyContent={"space-between"} alignItems={"center"}>
            <Grid item>
              <Stack flexDirection={'row'}>
                <Button component={Link}
                        to={HOME_PATH}
                        sx={{color: "inherit"}}
                >
                  Home
                </Button>
              </Stack>
            </Grid>
            {profile ? (
              <div className={s.mainUserFunc}>
                <Link to={ADD_PLACE_PATH} className={s.mainUserLinks}>Add new place</Link>
                <ProfileMenu
                  handleLogout={() => {
                  dispatch(userLogout());
                }}
                  profileImg={profile?.image ? /http/.test(profile.image) ? `url(${profile.image})` : `url(${BASE_URL + profile.image})` : `url(${EmptyAvatar})`}
                  username={profile.username}
                  locationTo={GALLERY_LOCATION + profile._id}
                  />
              </div>
            ) : (
              <Grid display={"inline-flex"}>
                <Grid item>
                  <Button component={Link}
                          to={LOGIN_PATH}
                          sx={{color: "inherit"}}
                  >
                    Sign in
                  </Button>
                </Grid>
                <Grid item>
                  <Divider orientation="vertical" sx={{backgroundColor: 'gainsboro',}}/>
                </Grid>
                <Grid item>
                  <Button component={Link}
                          to={REG_PATH}
                          sx={{color: "inherit"}}
                  >
                    Sign up
                  </Button>
                </Grid>
              </Grid>
            )
            }
          </Grid>
        </Toolbar>
      </AppBar>
      <Toolbar/>
    </>
  );
};

const useStyles = makeStyles((theme) => ({
  mainUserFunc: {
    display: "flex",
    alignItems: "center",
  },
  mainUserLinks: {
    color: theme.palette.grey["100"],
    padding: '0 20px',
    textDecoration: "none",
  },
}));

export default AppToolbar;