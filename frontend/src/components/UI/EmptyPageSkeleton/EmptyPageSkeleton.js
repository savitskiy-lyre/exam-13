import * as React from 'react';
import Skeleton from '@mui/material/Skeleton';
import Stack from '@mui/material/Stack';
import {Grid} from "@mui/material";

const EmptyPageSkeleton = () => {
  const Row = (
    <Grid container spacing={2}>
      <Grid item xs={4}>
        <Skeleton variant="rectangular" width={210} height={118}/>
      </Grid>
      <Grid item xs={4}>
        <Skeleton variant="rectangular" width={210} height={118}/>
      </Grid>
      <Grid item xs={4}>
        <Skeleton variant="rectangular" width={210} height={118}/>
      </Grid>
    </Grid>);

  return (
    <Stack spacing={3}>
      {Row}
      {Row}
      {Row}
      {Row}
    </Stack>
  );
};

export default EmptyPageSkeleton