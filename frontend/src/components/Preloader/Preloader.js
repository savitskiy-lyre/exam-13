import React from 'react';
import {LinearProgress} from "@mui/material";
import {makeStyles} from "@mui/styles";

const Preloader = () => {
  const s = useStyles();
  return (
    <div className={s.root}>
      <LinearProgress/>
      <div className={s.spacer}/>
      <LinearProgress color={"secondary"}/>
      <div className={s.spacer}/>
      <LinearProgress color={"success"}/>
      <div className={s.spacer}/>
      <LinearProgress color={"info"}/>
      <div className={s.spacer}/>
      <LinearProgress color={'error'}/>
      <div className={s.spacer}/>
      <LinearProgress color={'warning'}/>
      <div className={s.spacer}/>
      <LinearProgress/>
    </div>
  );
};

const useStyles = makeStyles({
  root: {
    margin: '30% 0'
  },
  spacer: {
      paddingTop: '50px'
  }
})


export default Preloader;