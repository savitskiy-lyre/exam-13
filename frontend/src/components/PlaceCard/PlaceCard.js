import React from 'react';
import {Stack, Tooltip} from "@mui/material";
import CardMedia from "@mui/material/CardMedia";
import {Link} from "react-router-dom";
import RatingStars from "../RatingStars/RatingStars";
import Typography from "@mui/material/Typography";
import PhotoCameraIcon from "@mui/icons-material/PhotoCamera";
import Card from "@mui/material/Card";
import {makeStyles} from "@mui/styles";
import {BASE_URL} from "../../config";
import no_img from "../../assets/images/no_img.png";
import {Carousel} from "react-responsive-carousel";
import {PLACE_INFO_PATH} from "../../constants/paths";
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';

const PlaceCard = ({data, isAdmin = false, onDeleteHandler}) => {
  const s = useStyles();

  const onSlideHandler = (i, el) => {
    const src = el.props.src;
    console.log(src);
  };

  return (
    <Card sx={{minWidth: 200, maxWidth: 280, position: 'relative'}}>
      {isAdmin && (
        <Tooltip title={'Delete place'}>
          <IconButton onClick={onDeleteHandler} sx={{position: 'absolute', right: 0, bottom: 0, zIndex: 1000}} color={"error"}>
            <DeleteIcon/>
          </IconButton>
        </Tooltip>
      )}

      <div>
        <Carousel autoplay
                  showStatus={false}
                  showThumbs={false}
                  showIndicators={false}
                  onClickItem={onSlideHandler}
        >
          {(data?.images?.length > 0) ? (
            data.images.map((img, i) => (
              <CardMedia
                component={'img'}
                height={180}
                key={data._id + i + 'image'}
                src={BASE_URL + '/' + img}
                alt="pic"
              />
            ))
          ) : (
            <CardMedia
              component={'img'}
              height={180}
              src={no_img}
              sx={{objectFit: 'contain'}}
              alt="pic"
            />
          )}
        </Carousel>
      </div>
      <Stack p={1} spacing={1} alignItems={"flex-start"}>
        <Link to={PLACE_INFO_PATH + data._id}>
          {data.title}
        </Link>
        <RatingStars readOnly defaultValue={data.rating}/>
        <Typography>
          ({data.rating}, {data.reviews.length} reviews)
        </Typography>
        <div className={s.photosText}>
          <PhotoCameraIcon/>
          <Typography>
            {data.images.length} photos
          </Typography>
        </div>
      </Stack>
    </Card>
  );
};

const useStyles = makeStyles({
  photosText: {
    display: "flex",
    alignItems: "center",
    '& svg': {
      marginRight: 10,
    }
  },
});

export default PlaceCard;