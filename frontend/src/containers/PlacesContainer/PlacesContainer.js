import React, {useEffect} from 'react';
import {Grid, Stack} from "@mui/material";
import PlaceCard from "../../components/PlaceCard/PlaceCard";
import {fetchPlaces, fetchPlacesSuccess, removePlace} from "../../store/actions/placeActions";
import {useDispatch, useSelector} from "react-redux";
import Preloader from "../../components/Preloader/Preloader";

const PlacesContainer = () => {
  const dispatch = useDispatch();
  const places = useSelector(state => state.places.data);
  const loadingFetch = useSelector(state => state.places.loadingFetch);
  const profile = useSelector(state => state.profile.data);

  const onDeleteCard = (id) => {
    dispatch(removePlace(id));
  }

  useEffect(() => {
    dispatch(fetchPlaces());
    return () => {
      dispatch(fetchPlacesSuccess(null));
    }
  }, [dispatch])

  return (
    <Stack my={3}>
      {(!places || loadingFetch) && (
        <Preloader/>
      )}
      <Grid container spacing={1} flexGrow={1} alignContent={"flex-start"} justifyContent={"center"}>
        {places?.length > 0 && (
          places.map((place) => {
            return (
              <Grid item key={place._id}>
                <PlaceCard data={place}
                           isAdmin={profile?.role === 'admin'}
                           onDeleteHandler={() => {
                             onDeleteCard(place._id)
                           }}
                />
              </Grid>
            )
          })
        )}
      </Grid>
    </Stack>
  );
};


export default PlacesContainer;