import React, {useEffect, useState} from 'react';
import {Container, Stack, TextField, Tooltip} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {addPlaceReview, fetchPlace, fetchPlaceSuccess, removePlaceReview} from "../../store/actions/placeActions";
import {useParams} from "react-router-dom";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import CardMedia from "@mui/material/CardMedia";
import {BASE_URL} from "../../config";
import no_img from "../../assets/images/no_img.png";
import {Carousel} from "react-responsive-carousel";
import RatingStars from "../../components/RatingStars/RatingStars";
import {toast} from "react-toastify";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import Preloader from "../../components/Preloader/Preloader";
import {LoadingButton} from "@mui/lab";

const initialRate = {
  quality_food: '0',
  quality_interior: '0',
  quality_service: '0',
};

const PlaceReviewContainer = () => {
  const dispatch = useDispatch();
  const {id} = useParams();
  const place = useSelector(state => state.places.target);
  const loadingAddReview = useSelector(state => state.places.loadingAddReview);
  const profile = useSelector(state => state.profile.data);
  const [revRate, setRevRate] = useState(initialRate);
  const [revMsg, setRevMsg] = useState('');

  const onRevMsgChanger = (e) => {
    setRevMsg(e.target.value);
  }

  const onRevRateChanger = (e) => {
    setRevRate(prev => ({...prev, [e.target.name]: e.target.value}));
  }

  const onRemovePlaceReview = (id, placeId) => {
    dispatch(removePlaceReview({id, placeId}))
  }

  const onRevResultClick = () => {
    if (!revMsg) return toast.warning('Fill review message field !')
    dispatch(addPlaceReview({
      user: profile._id,
      placeID: id,
      message: revMsg,
      ratings: revRate,
    }))
    setRevMsg('')
  }

  useEffect(() => {
    dispatch(fetchPlace(id));
    return () => {
      dispatch(fetchPlaceSuccess(null));
    }
  }, [dispatch, id])

  return (
    <Container>
      <Stack my={3} spacing={2}>
        {(!place) && (
          <Preloader/>
        )}
        {place && (
          <>
            <Grid container>
              <Grid item container xs={12} md={6} flexGrow={2} py={2}>
                <Stack spacing={3} justifyContent={"space-between"} flexGrow={2}>
                  <div>
                    <Typography variant={'h2'}>
                      {place.title}
                    </Typography>
                    <Typography variant={'subtitle1'}>
                      {place.description}
                    </Typography>
                  </div>
                  <Stack spacing={2}>
                    <Typography variant={'h5'}>Rating :</Typography>
                    <RatingStars readOnly defaultValue={place?.rating}/>
                  </Stack>
                </Stack>
              </Grid>
              <Grid item xs={12} md={6}>
                <Carousel autoplay
                          showStatus={false}
                          showThumbs={false}
                          showIndicators={false}
                >
                  {(place?.images?.length > 0) ? (
                    place.images.map((img, i) => (
                      <CardMedia
                        component={'img'}
                        height={380}
                        key={place._id + i + 'image'}
                        src={BASE_URL + '/' + img}
                        alt="pic"
                      />
                    ))
                  ) : (
                    <CardMedia
                      component={'img'}
                      height={180}
                      src={no_img}
                      sx={{objectFit: 'contain'}}
                      alt="pic"
                    />
                  )}
                </Carousel>
              </Grid>
            </Grid>
            {profile && (
              <Stack spacing={2}>
                <Typography variant={'h5'}>
                  Add review :
                </Typography>
                <TextField
                  multiline
                  rows={5}
                  value={revMsg}
                  onChange={onRevMsgChanger}
                  label={'Message'}
                />
                <Grid container>
                  <Grid item xs={12} sm={6} md={4}>
                    <Stack flexDirection={"row"} alignItems={'center'} px={2}>
                      <Typography variant={'body1'}>
                        Quality Food :
                      </Typography>
                      <RatingStars onStarClick={onRevRateChanger} name={'quality_food'}/>
                    </Stack>
                  </Grid>
                  <Grid item xs={12} sm={6} md={4}>
                    <Stack flexDirection={"row"} alignItems={'center'} px={2}>
                      <Typography variant={'body1'}>
                        Quality Interior :
                      </Typography>
                      <RatingStars onStarClick={onRevRateChanger} name={'quality_interior'}/>
                    </Stack>
                  </Grid>
                  <Grid item xs={12} sm={6} md={4}>
                    <Stack flexDirection={"row"} alignItems={'center'} px={2}>
                      <Typography variant={'body1'}>
                        Quality Service :
                      </Typography>
                      <RatingStars onStarClick={onRevRateChanger} name={'quality_service'}/>
                    </Stack>
                  </Grid>
                </Grid>
                <LoadingButton loading={loadingAddReview} color={'success'} onClick={onRevResultClick}>Add review</LoadingButton>
              </Stack>
            )}
          </>
        )}
        {place?.reviews?.length > 0 && (
          <Stack spacing={2}>
            <Typography variant={'h5'}>
              Reviews :
            </Typography>
            {place.reviews.map((rev) => (
              <Stack spacing={2} sx={{border: '1px solid gainsboro', background: "white"}} p={2}
                     key={rev._id}>
                <Typography variant={'subtitle1'} sx={{position: 'relative'}}>
                  {profile?.role === 'admin' && (
                    <Tooltip title={'Delete Review'}>
                      <IconButton onClick={() => {
                        onRemovePlaceReview(rev._id, id)
                      }} sx={{position: 'absolute', right: -15, top: -15, zIndex: 1000}} color={"error"}>
                        <DeleteIcon/>
                      </IconButton>
                    </Tooltip>
                  )}
                  On {rev.created} <strong>{rev.user.username}</strong> said:
                </Typography>
                <Typography variant={'body1'}>
                  {rev.message}
                </Typography>
                <Stack>
                  <Stack flexDirection={"row"} alignItems={'center'} px={2}>
                    <Typography variant={'body1'} sx={{minWidth: 200}}>
                      Overall :
                    </Typography>
                    <RatingStars readOnly defaultValue={rev.ratings.overall}/>
                  </Stack>
                  <Stack flexDirection={"row"} alignItems={'center'} px={2}>
                    <Typography variant={'body1'} sx={{minWidth: 200}}>
                      Quality Food :
                    </Typography>
                    <RatingStars readOnly defaultValue={rev.ratings.quality_food}/>
                  </Stack>
                  <Stack flexDirection={"row"} alignItems={'center'} px={2}>
                    <Typography variant={'body1'} sx={{minWidth: 200}}>
                      Quality Interior :
                    </Typography>
                    <RatingStars readOnly defaultValue={rev.ratings.quality_interior}/>
                  </Stack>
                  <Stack flexDirection={"row"} alignItems={'center'} px={2}>
                    <Typography variant={'body1'} sx={{minWidth: 200}}>
                      Quality Service :
                    </Typography>
                    <RatingStars readOnly defaultValue={rev.ratings.quality_service}/>
                  </Stack>
                </Stack>
              </Stack>
            ))}
          </Stack>
        )}
      </Stack>
    </Container>
  );
};

export default PlaceReviewContainer;