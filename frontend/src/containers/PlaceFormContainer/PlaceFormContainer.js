import React, {useState} from 'react';
import {Checkbox, Container, FormControlLabel, Stack, TextField, Typography} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {addNewPlace} from "../../store/actions/placeActions";
import {LoadingButton} from "@mui/lab";

const initState = {
  title: '',
  description: '',
  access: false,
  images: []
}

const PlaceFormContainer = () => {
  const dispatch = useDispatch();
  const [state, setState] = useState(initState);
  const loadingAdd = useSelector(state => state.places.loadingAdd);

  const onFormValueChanger = (e) => {
    if (e.target.name === 'access') {
      return setState(prev => ({...prev, [e.target.name]: e.target.checked}))
    }
    setState(prev => ({...prev, [e.target.name]: e.target.value}))
  }
  const onFormImgChanger = (e) => {
    setState(prev => ({...prev, [e.target.name]: e.target.files}))
  }

  const onSubmitHandler = (e) => {
    e.preventDefault();

    const newUserFormData = new FormData();
    Object.keys(state).forEach(key => {
      if (key === 'images') return
      newUserFormData.append(key, state[key]);
    })

    for (let i = 0; i < state.images.length; i++) {
      newUserFormData.append('images', state.images[i]);
    }

    dispatch(addNewPlace(newUserFormData));
  };

  return (
    <Container sx={{my: '10%'}}>
      <Stack component={'form'} onSubmit={onSubmitHandler} my={3} spacing={2}>
        <Typography variant={'h5'}>
          Add new place
        </Typography>
        <TextField
          label={'Title'}
          name={'title'}
          onChange={onFormValueChanger}
          value={state.title}
          required
        />
        <TextField
          label={'Description'}
          name={'description'}
          onChange={onFormValueChanger}
          value={state.description}
          multiline
          rows={8}
          required
        />
        <TextField
          type={"file"}
          name={'images'}
          inputProps={{multiple: true}}
          onChange={onFormImgChanger}
          required
        />
        <FormControlLabel
          sx={{textAlign: "center"}}
          value="end"
          control={<Checkbox name={'access'} value={state.access} onChange={onFormValueChanger} required/>}
          label="By sumbitting this form, you agree that the following information will be submitted to the public domain, and administrators of this site will have full control over the said information."
          labelPlacement="bottom"
        />
        <LoadingButton loading={loadingAdd} type={'submit'} color={"success"} variant={"contained"}
                       sx={{maxWidth: 500, alignSelf: 'center', width: '100%'}}
        >
          Submit new place
        </LoadingButton>
      </Stack>
    </Container>
  );
};

export default PlaceFormContainer;