export const LOGIN_PATH = '/signin';
export const REG_PATH = '/signup';
export const ADD_PLACE_PATH = '/place/add';
export const PLACE_INFO_PATH = '/places/';
export const PLACE_INFO_PATH_ID = '/places/:id';
export const HOME_PATH = '/';