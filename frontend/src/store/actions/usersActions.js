import usersSlice from "../slices/usersSlice";

export const {
  registerUser,
  registerUserSuccess,
  registerUserFailure,
  facebookLogin,
  googleLogin,
  signIn,
  signInSuccess,
  signInFailure,
  userLogout,
  userLogoutSuccess,

} = usersSlice.actions;