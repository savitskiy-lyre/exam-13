import placeSlice from "../slices/placeSlice";

export const {
  fetchPlaces,
  fetchPlacesSuccess,
  fetchPlacesFailure,
  fetchPlace,
  fetchPlaceSuccess,
  fetchPlaceFailure,
  addNewPlace,
  addNewPlaceSuccess,
  addNewPlaceFailure,
  addPlaceReview,
  addPlaceReviewSuccess,
  addPlaceReviewFailure,
  removePlace,
  removePlaceSuccess,
  removePlaceFailure,
  removePlaceReview,
  removePlaceReviewSuccess,
  removePlaceReviewFailure,
} = placeSlice.actions;