import {combineReducers} from "redux";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import createSagaMiddleware from 'redux-saga';
import thunk from "redux-thunk";
import {configureStore} from "@reduxjs/toolkit";
import usersSlice from "./slices/usersSlice";
import placeSlice from "./slices/placeSlice";
import {rootSagas} from "./rootSagas";

const rootReducer = combineReducers({
  profile: usersSlice.reducer,
  places: placeSlice.reducer,
});
const persistedState = loadFromLocalStorage();

const sagaMiddleware = createSagaMiddleware();

const middleware = [
  thunk,
  sagaMiddleware,
];

const store = configureStore({
  reducer: rootReducer,
  middleware,
  devTools: true,
  preloadedState: persistedState
});

store.subscribe(() => {
  saveToLocalStorage({
    profile: store.getState().profile,
  });
})

sagaMiddleware.run(rootSagas);

export default store;