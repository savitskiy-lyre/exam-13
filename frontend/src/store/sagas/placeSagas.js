import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {PLACES_REVIEW_URL, PLACES_URL} from "../../config";
import {toast} from "react-toastify";
import {
  addNewPlace,
  addNewPlaceFailure,
  addNewPlaceSuccess,
  addPlaceReview,
  addPlaceReviewFailure,
  addPlaceReviewSuccess,
  fetchPlace,
  fetchPlaceFailure,
  fetchPlaces,
  fetchPlacesFailure,
  fetchPlacesSuccess,
  fetchPlaceSuccess,
  removePlace,
  removePlaceFailure,
  removePlaceReview,
  removePlaceReviewFailure,
  removePlaceReviewSuccess,
  removePlaceSuccess
} from "../actions/placeActions";
import {historyPush} from "../actions/historyActions";
import {HOME_PATH} from "../../constants/paths";

export function* fetchPlacesSaga() {
  try {
    const {data} = yield axiosApi.get(PLACES_URL);
    yield put(fetchPlacesSuccess(data));
  } catch (err) {
    if (!err.response) toast.error(err.message);
    yield put(fetchPlacesFailure(err?.response?.data));
  }
}

export function* fetchPlaceSaga({payload}) {
  try {
    const {data} = yield axiosApi.get(PLACES_URL + '/' + payload);
    yield put(fetchPlaceSuccess(data));
  } catch (err) {
    if (!err.response) toast.error(err.message);
    yield put(fetchPlaceFailure(err?.response?.data));
  }
}

export function* addNewPlaceSaga({payload}) {
  try {
    yield axiosApi.post(PLACES_URL, payload);
    yield put(addNewPlaceSuccess());
    yield put(historyPush(HOME_PATH));
  } catch (err) {
    if (!err.response) toast.error(err.message);
    if (err?.response?.data) toast.error(err.response.data?.message);
    yield put(addNewPlaceFailure(err?.response?.data));
  }
}

export function* addPlaceReviewSaga({payload}) {
  try {
    yield axiosApi.post(PLACES_REVIEW_URL, payload);
    yield put(fetchPlace(payload.placeID));
    yield put(addPlaceReviewSuccess());
    toast.success('Added successfully');
  } catch (err) {
    if (!err.response) toast.error(err.message);
    if (err?.response?.data) toast.error(err.response.data?.message);
    yield put(addPlaceReviewFailure(err?.response?.data));
  }
}

export function* removePlaceSaga({payload}) {
  try {
    yield axiosApi.delete(PLACES_URL + '/' + payload);
    yield put(fetchPlaces());
    yield put(removePlaceSuccess());
  } catch (err) {
    if (!err.response) toast.error(err.message);
    if (err?.response?.data) toast.error(err.response.data?.message);
    yield put(removePlaceFailure(err?.response?.data));
  }
}

export function* removePlaceReviewSaga({payload}) {
  try {
    yield axiosApi.delete(PLACES_REVIEW_URL + '/' + payload.id);
    yield put(fetchPlace(payload.placeId));
    yield put(removePlaceReviewSuccess());
  } catch (err) {
    if (!err.response) toast.error(err.message);
    if (err?.response?.data) toast.error(err.response.data?.message);
    yield put(removePlaceReviewFailure(err?.response?.data));
  }
}

const placeSaga = [
  takeEvery(fetchPlaces, fetchPlacesSaga),
  takeEvery(fetchPlace, fetchPlaceSaga),
  takeEvery(addPlaceReview, addPlaceReviewSaga),
  takeEvery(addNewPlace, addNewPlaceSaga),
  takeEvery(removePlace, removePlaceSaga),
  takeEvery(removePlaceReview, removePlaceReviewSaga),
];

export default placeSaga;