import {all} from 'redux-saga/effects';
import usersSagas from "./sagas/usersSagas";
import placeSagas from "./sagas/placeSagas";

export function* rootSagas() {
  yield all([
    ...usersSagas,
    ...placeSagas,
  ])
}