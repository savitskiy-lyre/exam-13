import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
  data: null,
  target: null,
  loadingFetch: false,
  loadingAdd: false,
  loadingAddReview: false,
  loadingTargetFetch: false,
  loadingRemovePlace: false,
  loadingRemovePlaceReview: false,
  errRemovePlaceReview: null,
  errTargetFetch: null,
  errRemovePlace: null,
  errAddReview: null,
  errFetch: null,
  errAdd: null,
};

const name = 'place';

const placeSlice = createSlice({
  name,
  initialState,
  reducers: {
    fetchPlaces(state) {
      state.loadingFetch = true;
      state.errFetch = null;
    },
    fetchPlacesSuccess(state, {payload}) {
      state.loadingFetch = false;
      state.data = payload;
    },
    fetchPlacesFailure(state, {payload}) {
      state.loadingFetch = false;
      state.errFetch = payload;
    },
    addNewPlace(state) {
      state.loadingAdd = true;
      state.errAdd = null;
    },
    addNewPlaceSuccess(state) {
      state.loadingAdd = false;
    },
    addNewPlaceFailure(state, {payload}) {
      state.loadingAdd = false;
      state.errAdd = payload;
    },
    fetchPlace(state) {
      state.loadingTargetFetch = true;
      state.errTargetFetch = null;
    },
    fetchPlaceSuccess(state, {payload}) {
      state.loadingTargetFetch = false;
      state.target = payload;
    },
    fetchPlaceFailure(state, {payload}) {
      state.loadingTargetFetch = false;
      state.errTargetFetch = payload;
    },
    addPlaceReview(state) {
      state.loadingAddReview = true;
      state.errAddReview = null;
    },
    addPlaceReviewSuccess(state) {
      state.loadingAddReview = false;
    },
    addPlaceReviewFailure(state, {payload}) {
      state.loadingAddReview = false;
      state.errAddReview = payload;
    },
    removePlace(state) {
      state.loadingRemovePlace = true;
      state.errRemovePlace = null;
    },
    removePlaceSuccess(state) {
      state.loadingRemovePlace = false;
    },
    removePlaceFailure(state, {payload}) {
      state.loadingRemovePlace = false;
      state.errRemovePlace = payload;
    },
    removePlaceReview(state) {
      state.loadingRemovePlaceReview = true;
      state.errRemovePlaceReview = null;
    },
    removePlaceReviewSuccess(state) {
      state.loadingRemovePlaceReview = false;
    },
    removePlaceReviewFailure(state, {payload}) {
      state.loadingRemovePlaceReview = false;
      state.errRemovePlaceReview = payload;
    },
  }
});

export default placeSlice;