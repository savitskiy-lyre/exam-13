const User = require('../models/User');

const authUserToken = async (req, res, next) => {
   const reqToken = req.get('Authorization');
   if (!reqToken) return res.status(401).send({error: 'Wrong authorization token'});
   try {
      const user = await User.findOne({token: reqToken});
      if (!user) return res.status(401).send({error: 'Wrong authorization token'});
      req.user = user;
   } catch (err) {
      return res.status(500).send({error: 'Internal Server Error'});
   }
   next();
}

module.exports = authUserToken;