const mongoose = require("mongoose");

const RateSchema = new mongoose.Schema({
  overall: {
    type: Number,
    min: 0,
    max: 5,
  },
  quality_service: {
    type: Number,
    required: true,
    min: 0,
    max: 5,
  },
  quality_food: {
    type: Number,
    required: true,
    min: 0,
    max: 5,
  },
  quality_interior: {
    type: Number,
    required: true,
    min: 0,
    max: 5,
  }
});

const ReviewSchema = new mongoose.Schema({
  user: {
    type: mongoose.Types.ObjectId,
    ref: 'users',
    required: true,
    unique: true,
  },
  ratings: {
    type: RateSchema,
    required: true,
  },
  message: {
    type: String,
    required: true,
  },
  created: {
    type: Date,
    default: new Date(),
  }
});

const PlaceSchema = new mongoose.Schema({
  creator: {
    type: mongoose.Types.ObjectId,
    ref: 'users',
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  images: {
    type: [String],
    required: true,
  },
  rating: {
    type: Number,
    default: 0,
    min: 0,
    max: 5,
  },
  reviews: {
    type: [ReviewSchema],
  },
  created: {
    type: Date,
    default: new Date(),
  }
})

RateSchema.pre('save', function (next) {
  const [f, i, s] = [this.quality_food, this.quality_interior, this.quality_service];
  const preResInt =(f + i + s) / 3;
  this.overall = Math.round(preResInt * 10) / 10;

  next();
})

PlaceSchema.pre('save', function (next) {
  if (this.reviews.length > 0) {
    const preDef = this.reviews.reduce((res, curr) => {
      return res + curr.ratings.overall;
    }, 0)
    this.rating = Math.round(preDef * 10 / this.reviews.length) / 10;
  }

  next();
})

const PlaceModel = mongoose.model('place', PlaceSchema);
module.exports = PlaceModel;