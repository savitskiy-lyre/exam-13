const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const PlaceModel = require("./models/PlaceModel");


const run = async () => {
  await mongoose.connect(config.db.testStorageUrl)

  const collections = await mongoose.connection.db.listCollections().toArray();
  for (const collection of collections) {
    await mongoose.connection.db.dropCollection(collection.name);
  }

  const [user1, user2, user3] = await User.create({
    username: 'Ivan I',
    password: '123',
    email: 'Ivan@good.com',
    image: '/fixtures/user.svg',
    token: nanoid(),
  }, {
    username: 'Ivan II',
    password: '123',
    email: 'IvanII@good.com',
    image: '/fixtures/user.svg',
    token: nanoid(),
  }, {
    username: 'Pavel',
    password: '123',
    email: 'Pavel@good.com',
    image: '/fixtures/user.svg',
    token: nanoid(),
  }, {
    username: 'admin',
    password: '123',
    email: 'admin@good.com',
    image: '/fixtures/moder.jpg',
    token: nanoid(),
    role: 'admin',
  })

  await PlaceModel.create({
      creator: user1,
      title: 'Tree',
      description: 'Best place in the world',
      images: ['/fixtures/img13.jpg', '/fixtures/img14.jpg'],
      created: new Date(),
      reviews: [{
        user: user1,
        ratings: {
          quality_service: 4.5,
          quality_food: 5,
          quality_interior: 0,
        },
        message: 'Hello, good thing',
        created: new Date(),
      }, {
        user: user2,
        ratings: {
          quality_service: 1.5,
          quality_food: 5,
          quality_interior: 0,
        },
        message: 'Hello, good thing',
        created: new Date(),
      }, {
        user: user3,
        ratings: {
          quality_service: 2.5,
          quality_food: 5,
          quality_interior: 0,
        },
        message: 'Hello, good thing',
        created: new Date(),
      }],
    }, {
      creator: user2,
      title: 'Tree',
      description: 'Best place in the world',
      images: ['/fixtures/img15.jpg', '/fixtures/img16.jpg'],
      created: new Date(),
    }, {
      creator: user3,
      title: 'Tree',
      description: 'Best place in the world',
      images: ['/fixtures/img17.jpg', '/fixtures/img18.jpg', '/fixtures/img19.jpg'],
      created: new Date(),
    },
  )

  await mongoose.connection.close();
}
run().catch(console.error)
