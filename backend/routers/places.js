const express = require("express");
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");
const PlaceModel = require("../models/PlaceModel");
const UserModel = require("../models/User");
const authUserToken = require("../middleware/authUserToken");
const permit = require("../middleware/permit");
const ApiError = require("../exceptions/api-error");
const mongoose = require("mongoose");
const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    const places = await PlaceModel.find().populate('reviews.user');
    res.send(places);
  } catch (err) {
    next(err)
  }
});

router.get('/:id', async (req, res, next) => {
  const {id} = req.params;
  try {
    if (!id) {
      throw ApiError.BadRequest('Required identifier not found')
    }
    const place = await PlaceModel.findById(id).populate('reviews.user');
    res.send(place);
  } catch (err) {
    next(err)
  }
});

router.post('/', authUserToken, upload.any('images'), async (req, res, next) => {
  try {
    if (!req.body.access) {
      throw ApiError.BadRequest('User does not grant access')
    }
    const newPlace = {
      creator: req.user,
      title: req.body.title,
      description: req.body.description,
      images: [],
    };

    if (req.files) {
      req.files.forEach(f => newPlace.images.push('/uploads/' + f.filename));
    }
    const newPl = new PlaceModel(newPlace);
    await newPl.save();
    res.send(newPl);

  } catch (err) {
    next(err)
  }

});

router.post('/review', authUserToken, async (req, res, next) => {
  try {
    const {ratings, placeID, user: userID, message} = req.body;
    const place = await PlaceModel.findById(placeID);
    const userDb = await UserModel.findById(userID);

    if (!userDb) {
      throw ApiError.BadRequest('Not existent user')
    }
    place.reviews.push({user: userDb._id, message, ratings})
    place.save();

    res.send(place)
  } catch (err) {
    next(err)
  }

});

router.delete('/:id', authUserToken, permit('admin'), upload.any('images'), async (req, res, next) => {
  try {
    const {id} = req.params;
    if (!id) {
      throw ApiError.BadRequest('Not existent id')
    }
    const result = await PlaceModel.findByIdAndDelete(id);
    res.send(result);

  } catch (err) {
    next(err)
  }

});

router.delete('/review/:id', authUserToken, permit('admin'), upload.any('images'), async (req, res, next) => {
  try {
    const {id} = req.params;
    if (!id) {
      throw ApiError.BadRequest('Not existent id')
    }
    const place = await PlaceModel.findOne({'reviews._id': {$in: mongoose.Types.ObjectId(id)}})
    place.reviews = place.reviews.filter((el)=>{return el._id.toString() !== id})
    place.save();
    res.send(place);
  } catch (err) {
    next(err)
  }

});

module.exports = router;